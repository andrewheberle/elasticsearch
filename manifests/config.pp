class elasticsearch::config inherits elasticsearch {
  if $elasticsearch::config_manage {
    notify { 'Config management is not yet implemented.': }
  }
}