class elasticsearch (
    $repository_manage,
    $repository_ensure,
    $repository_type,
    $repository_key_source,
    $repository_version,
    $package_ensure,
    $package_name,
    $package_manage,
    $java_package_ensure,
    $java_package_name,
    $java_package_manage,
    $config_manage,
    $service_enable,
    $service_ensure,
    $service_manage,
    $service_name,
    $config_manage,
  ) {

  anchor { 'elasticsearch::begin': } ->
  class { '::elasticsearch::repository': } ->
  class { '::elasticsearch::install': } ->
  class { '::elasticsearch::config': } ~>
  class { '::elasticsearch::service': } ->
  anchor { 'elasticsearch::end': }
}
