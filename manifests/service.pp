class elasticsearch::service inherits elasticsearch {
  if $elasticsearch::service_manage {
    service {$elasticsearch::service_name:
      ensure => $elasticsearch::service_ensure,
      enable => $elasticsearch::service_enable,
    }
  }
}