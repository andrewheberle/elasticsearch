class elasticsearch::install inherits elasticsearch {
  if $elasticsearch::package_manage {
    if $elasticsearch::java_package_manage {
      package { $elasticsearch::java_package_name:
        ensure => $elasticsearch::java_package_ensure,
        before => Package[$elasticsearch::package_name],
      }
    }

    package { $elasticsearch::package_name:
      ensure => $elasticsearch::package_ensure,
    }
  }
}