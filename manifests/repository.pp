class elasticsearch::repository inherits elasticsearch {
  if $elasticsearch::repository_manage {
    case $elasticsearch::repository_type {
      'yum': {
        yumrepo { 'elasticsearch':
          ensure   => $elasticsearch::repository_ensure,
          name     => "elasticsearch-${elasticsearch::repository_version}",
          descr    => "elasticsearch repository for ${elasticsearch::repository_version} packages",
          baseurl  => "http://packages.elastic.co/elasticsearch/${elasticsearch::repository_version}/centos",
          gpgcheck => true,
          gpgkey   => $elasticsearch::repository_key_source,
          enabled  => true,
        }
      }
      'apt': {
        include apt
        apt::source { 'elasticsearch':
          comment  => "elasticsearch repository for ${elasticsearch::repository_version} packages",
          location => "http://packages.elastic.co/elasticsearch/${elasticsearch::repository_version}/debian",
          release  => 'stable',
          repos    => 'main',
          key      => {
            'id'     => '46095ACC8548582C1A2699A9D27D666CD88E42B4',
            'server' => 'pgp.mit.edu',
            'source' => $elasticsearch::repository_key_source,
          },
          include  => {
            'deb' => true,
            'src' => false,
          },
        }
      }
      default: {
        fail("Module ${module_name} is only supported with apt or yum based distros")
      }
    }
  }
}